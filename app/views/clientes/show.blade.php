<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
       {{ HTML::style('css/bootstrap.css'); }}
       <link href='/css/style.css' type="text/css" rel="stylesheet"> 
       <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
       <title>Clientes Hotel</title>
</head>


<body>
<header >
   <a href="/">  <img src="/usuarios.png" align="left"style="width:42px;height:42px;border:0"> </a>
    <h4   align="right"><a href="/logout">Cerrar sesión</a></h4>
    <h1   align="center" style="color:#6f3a2a;">Clientes</h1>
 </header>
	     <div style="width:150px;height:200px;" > 
	         <div style="width:150px;height:200px;float:left;margin:0 7px 7px 7px;">
				      <a href="/clientes/add"> Agregar Cliente</a></br>
				      <a href="/clientes/show">Listar Cliente </a>
			     </div>
        </div>

	<div class="container" style="padding: 0em 0em;margin: 0em 5%;">
		<div class="panel panel-default" style="text-aling: left;padding: 1em 0em; margin: 0em 5%;">
		
       <div class="panel-body" style="text-aling: left;" >
  					<table class="table table-striped">
    					<tr>
        					<th>Nombre</th>
        					<th>Apellido</th>
       					 	<th>Documento identidad</th>
          					<th>Ciudad de residencia</th>
        					<th>Profesion</th>
        					<th>Telefono Fijo</th>
        					<th>Celular</th>

    					</tr>
    				@foreach ($clients as $client)
    					<tr>
        					<td >{{ $client->first_name }}</td>
        					<td>{{ $client->last_name }}</td>
        					<td>{{ $client->identity_document }}</td>
				        	<td>{{ $client->location_name }}</td>
        					<td>{{ $client->profession }}</td>
        					<td>{{ $client->fixed_telephone }}</td>
        					<td>{{ $client->mobile_phone }}</td>
                  <th>{{Form::label('email', $client->email);}}</th>
        					<td><button class="btn btn-primary btn-lg"  data-toggle="modal" data-target=".bs-example-modal-lg">
                     {{HTML::link('/clientes/update/'.$client->client_id,'Actualizar cliente')}}
                     </button>
                  </td>
   		     				<td>
                  <button class="btn btn-primary btn-lg" data-toggle="modal" data-target=".bs-example-modal-lg">
                      {{HTML::link('clientes/delete/'.$client->client_id,'Borrar cliente')}}
                    </button>
                  </td>
    					</tr>
   					@endforeach
	 </table>
	</div>
</div>
</div>

 <script src="https://code.jquery.com/jquery.js"></script>
        {{ HTML::script('js/bootstrap.js'); }}

</body>

</html>